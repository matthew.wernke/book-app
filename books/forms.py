from django import forms
from books.models import Book, Magazine

class BookForm(forms.ModelForm):
  class Meta:
    model = Book
    exclude = []


    # fields = [
    #   "title",
    #   "author",
    #   "pages",
    #   "isbn",
    #   "year_published",
    #   "description",
    #   "image",
    #   "in_print",
    # ]


class MagazineForm(forms.ModelForm):
  class Meta:
    model = Magazine
    exclude = []
