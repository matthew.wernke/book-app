from django.db import models
from django.forms import DateInput
from django.contrib.auth.models import User

# Create your models here.
class Book(models.Model):
  title = models.CharField(max_length=200, unique=True)
  authors = models.ManyToManyField("Author", related_name="books")
  pages = models.SmallIntegerField(null=True)
  isbn = models.BigIntegerField(null=True)
  year_published = models.SmallIntegerField(null=True)
  description = models.TextField(null=True)
  image = models.URLField(null=True, blank=True)
  in_print = models.BooleanField(null=True)

  def __str__(self):
    return self.title + " by " + str(self.authors.first())

class Author(models.Model):
  name = models.CharField(max_length=200, unique=True)

  def __str__(self):
    return self.name

class BookReview(models.Model):
  reviewer = models.ForeignKey(User, related_name="reviews_by_this_user", on_delete=models.CASCADE)
  book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
  text = models.TextField()

  def __str__(self):
    return str(self.book)

class Magazine(models.Model):
  creator = models.ForeignKey(User, related_name="magazines", on_delete=models.CASCADE)
  title = models.CharField(max_length=200, unique=True)
  release_cycle = models.CharField(max_length=200)
  description = models.TextField(null=True)
  image = models.URLField(null=True, blank=True)
  genres = models.ManyToManyField("Genre", related_name="magazines")

  def __str__(self):
    return self.title

class Genre(models.Model):
  name = models.CharField(max_length=200, unique=True)

  def __str__(self):
    return self.name

class Issue(models.Model):
  title = models.CharField(max_length=200, unique=True)
  description = models.TextField(null=True)
  date_published = models.DateField(null=True)
  page_count = models.SmallIntegerField(null=True)
  issue_number = models.SmallIntegerField(null=True)
  cover_image = models.URLField(null=True, blank=True)
  magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)

  def __str__(self):
    return str(self.issue_number) + " | " + self.title
