from django.shortcuts import get_object_or_404, render, redirect
from books.models import Book, Magazine, Genre
from books.forms import BookForm, MagazineForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required #decorator function
def list_reviews(request):
  # how to get user who made the request?
  book_reviews = request.user.reviews_by_this_user.all()
  # how to get all model instances made by that user?
  context = {
    "reviews": book_reviews
  }

  return render(request, 'books/review_list.html', context)


def show_books(request):
  books = Book.objects.all()
  context = {
    "books": books
  }
  return render(request, "books/list.html", context)

def create_book_view(request):
  context = {}

  form = BookForm(request.POST or None)
  if form.is_valid():
    form.save()
    return redirect("show_books")


  context['form'] = form
  return render(request, "books/create.html", context)

def show_book(request, pk):
  context = {
    "book": Book.objects.get(pk=pk) if Book else None,
  }
  return render(request, "books/detail.html", context)

def update_book(request, pk):
  if Book and BookForm:
    instance = Book.objects.get(pk=pk)
    if request.method == "POST":
      form = BookForm(request.POST, instance=instance)
      if form.is_valid():
        form.save()
        return redirect("show_books")
    else:
      form = BookForm(instance=instance)
  else:
    form = None
  context = {
    "form": form,
  }
  return render(request, "books/edit.html", context)

def delete_book(request, pk):
  context = {}
  obj = get_object_or_404(Book, pk=pk)
  if request.method == "POST":
    obj.delete()
    return redirect("show_books")
  return render(request, "books/delete.html", context)

@login_required
def show_magazines(request):
  magazines = request.user.magazines.all()
  context = {
    "magazines": magazines
  }
  return render(request, "magazines/list.html", context)

def show_magazine(request, pk):
  context = {
    "magazine": Magazine.objects.get(pk=pk) if Magazine else None,
  }
  return render(request, "magazines/detail.html", context)

def create_magazine_view(request):
  context = {}

  form = MagazineForm(request.POST or None)
  if form.is_valid():
    form.save()
    return redirect("show_magazines")

  context['form'] = form
  return render(request, "magazines/create.html", context)

def update_magazine(request, pk):
  if Magazine and MagazineForm:
    instance = Magazine.objects.get(pk=pk)
    if request.method == "POST":
      form = MagazineForm(request.POST, instance=instance)
      if form.is_valid():
        form.save()
        return redirect("show_magazines")
    else:
      form = MagazineForm(instance=instance)
  else:
    form = None
  context = {
    "form": form,
  }
  return render(request, "magazines/edit.html", context)

def delete_magazine(request, pk):
  context = {}
  obj = get_object_or_404(Magazine, pk=pk)
  if request.method == "POST":
    obj.delete()
    return redirect("show_magazines")
  return render(request, "magazines/delete.html", context)

def show_genre(request, pk):
  genre = Genre.objects.get(pk=pk)
  # magazines = Magazine.objects.all()
  context = {
    "genre": genre,
    # "magazines": magazines,
  }
  return render(request, "magazines/genre.html", context)
