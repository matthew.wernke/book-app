from django.contrib import admin
from django.urls import path, include
from books.views import (
  delete_book,
  show_books,
  create_book_view,
  show_book,
  update_book,
  delete_book,
  list_reviews,
)


urlpatterns = [
  path('reviews/', list_reviews, name="list_reviews"),
  path("", show_books, name = "show_books"),
  path("create/", create_book_view, name = "create_book"),
  path("<int:pk>/", show_book, name="book_detail"),
  path("<int:pk>/edit/", update_book, name="edit_book"),
  path("<int:pk>/delete/", delete_book, name="delete_book"),
]
