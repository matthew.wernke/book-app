from django.contrib import admin
from books.models import Book, Magazine, Author, BookReview, Issue, Genre

# Register your models here.
# class BookAdmin(Book):
#   pass

# admin.site.register(BookAdmin)

admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(Author)
admin.site.register(BookReview)
admin.site.register(Issue)
admin.site.register(Genre)
