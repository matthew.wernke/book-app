from django.contrib import admin
from django.urls import path, include
from books.views import (
  show_genre,
  show_magazine,
  show_magazines,
  create_magazine_view,
  update_magazine,
  delete_magazine,
  show_genre,
)


urlpatterns = [
  path("", show_magazines, name = "show_magazines"),
  path("<int:pk>/", show_magazine, name = "magazine_detail"),
  path("create/", create_magazine_view, name = "create_magazine"),
  path("<int:pk>/edit/", update_magazine, name = "edit_magazine"),
  path("<int:pk>/delete/", delete_magazine, name = "delete_magazine"),
  path("genres/<int:pk>/", show_genre, name = "show_genre"),
]
